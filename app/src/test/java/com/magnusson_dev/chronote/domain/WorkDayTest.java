package com.magnusson_dev.chronote.domain;


import org.junit.Test;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;

import static org.junit.Assert.*;

public class WorkDayTest {


    private WorkDay testDay = new WorkDay.Builder(DayOfWeek.MONDAY, LocalDate.now()).build();

    private WorkDay.Builder testBuilder = new WorkDay.Builder(DayOfWeek.FRIDAY, LocalDate.now());

    private static final int NEGATIVE_VALUE = -1;
    private static final int VALID_NORMAL_HOURS = 8;
    private static final int VALID_OVERTIME_HOURS = 4;
    private static final int MAX_NR_HOURS = 24;


    @Test
    public void T00_failing_test() throws Exception {
        boolean success = true;
        assertTrue(success);
    }

    @Test
    public void T01_normalHoursInitiallyEqualsZero() throws Exception {
        assertTrue(testDay.normalHours() == 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void T02_normalHoursCannotBeNegativelyInitialized() throws Exception {
        testBuilder.normalHours(NEGATIVE_VALUE).build();
    }

    @Test
    public void T03_normalHoursChanged() throws Exception {
        testDay = testDay.addNormalHours(2);
        int priorHours = testDay.normalHours();
        testDay = testDay.addNormalHours(8);
        assertTrue(testDay.normalHours() == (8+priorHours)); // should evaluate to 10
    }

    /**
     * Test for exception being thrown
     * @throws Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void T04_normalHoursNegativeAddNotAllowed() throws Exception {
        testDay = testDay.addNormalHours(NEGATIVE_VALUE);
    }

    /**
     * Test for value being set properly
     * @throws Exception
     */
    @Test
    public void T05_normalHoursDefaultingIfNegativeInput() throws Exception {
        try {
            testDay = testBuilder.normalHours(NEGATIVE_VALUE).build();
        }
        catch(IllegalArgumentException iae){
            //
        }
        assertTrue(testDay.normalHours() != NEGATIVE_VALUE); // ergo == 0, as default val
    }

    @Test
    public void T06_normalHoursNotChangedIfAddNegativeInput() throws Exception {
        // if previous initialized value != 0
        testDay = testDay.addNormalHours(VALID_NORMAL_HOURS);
        try{
            testDay = testDay.addNormalHours(NEGATIVE_VALUE);
        }catch(IllegalArgumentException iae){
            //
        }
        assertTrue(testDay.normalHours() == VALID_NORMAL_HOURS);
    }

    @Test
    public void T07_overtimeHoursInitializedToZero() throws Exception {
        assertTrue(testDay.overtimeHours() == 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void T08_overtimeHoursCannotBeNegativalyInitialized() throws Exception {
        testDay = testBuilder.overtimeHours(NEGATIVE_VALUE).build();
    }

    @Test
    public void T09_overtimeHoursChanged() throws Exception {
        testDay = testDay.addOvertimeHours(VALID_OVERTIME_HOURS); // = 4
        int priorHours = testDay.overtimeHours();
        testDay = testDay.addOvertimeHours(2);
        assertTrue(testDay.overtimeHours() == (2+priorHours)); // should evaluate to 6
    }

    @Test(expected = IllegalArgumentException.class)
    public void T10_overtimeHoursNegativeAddNotAllowed() throws Exception {
        // testDay hours init set to 0
        testDay = testDay.addOvertimeHours(NEGATIVE_VALUE);
    }

    @Test
    public void T11_overtimeHoursDefaultingIfNegativeInput() throws Exception {
        try {
            testDay = testBuilder.overtimeHours(NEGATIVE_VALUE).build();
        }
        catch(IllegalArgumentException iae){
            //
        }
        assertTrue(testDay.normalHours() != NEGATIVE_VALUE); // ergo == 0, as default val
    }

    @Test
    public void T12_overtimeHoursNotChangedIfAddNegativeInput() throws Exception {
        // if previous initialized value != 0
        testDay = testDay.addOvertimeHours(VALID_OVERTIME_HOURS); // = 4
        try{
            testDay = testDay.addOvertimeHours(NEGATIVE_VALUE);
        }catch(IllegalArgumentException iae){
            //
        }
        assertTrue(testDay.overtimeHours() == VALID_OVERTIME_HOURS); // = 4
    }

    @Test
    public void T13_normalHoursChangedIfSubtractValidAmount() throws Exception {
        // if previous hours == 8
        testDay = testDay.addNormalHours(VALID_NORMAL_HOURS);
        testDay = testDay.subtractNormalHours(4);
        assertTrue(testDay.normalHours() == 4); // 8 - 4
    }

    @Test
    public void T14_normalHoursNotChangedIfSubtractInvalidAmount() throws Exception {
        // if previous value == 0, and subtraction is done
        try{
            testDay = testDay.subtractNormalHours(2);
        }catch(IllegalArgumentException iae){
            //iae.printStackTrace();
        }
        assertTrue(testDay.normalHours() == 0); // unchanged
    }

    @Test
    public void T15_normalHoursNotChangedIfAddAboveLimit() throws Exception {
        // add 24 hours + 1
        testDay = testDay.addNormalHours(VALID_NORMAL_HOURS); // 8 h
        try{
            testDay = testDay.addNormalHours(MAX_NR_HOURS); // 24 h added to previous 8 h (overflow)
        }catch(IllegalArgumentException iae){
            //iae.printStackTrace();
        }
        assertTrue(testDay.normalHours() == VALID_NORMAL_HOURS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void T16_normalHoursAddNotAllowedIfAboveLimit() throws Exception {
        // 24 is the limit
        testDay = testDay.addNormalHours(MAX_NR_HOURS +1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void T17_normalHoursInitializationNotAllowedIfAboveLimit() throws Exception {
        testBuilder.normalHours(MAX_NR_HOURS+1).build();
    }

    @Test
    public void T18_overtimeHoursNotChangedIfAddAboveLimit() throws Exception {
        testDay = testDay.addOvertimeHours(VALID_OVERTIME_HOURS); // 4 h
        try{
            testDay = testDay.addOvertimeHours(MAX_NR_HOURS); // 24 h added to previous 4 h (overflow)
        }catch (IllegalArgumentException iae){
           //iae.printStackTrace();
        }
        assertTrue(testDay.overtimeHours() == VALID_OVERTIME_HOURS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void T19_overtimeHoursInitializationNoAllowedIfAboveLimit() throws Exception {
        testBuilder.overtimeHours(MAX_NR_HOURS+1).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void T20_normalHoursAddNotAllowedIfExceedingSharedLimit() throws Exception {
        testDay = testDay.addOvertimeHours(20);
        testDay = testDay.addNormalHours(8); // maximum hours shared exceeded by 4 h
    }

    @Test(expected = IllegalArgumentException.class)
    public void T21_overtimeHoursAddNotAllowedIfExceedingSharedLimit() throws Exception {
        testDay = testDay.addNormalHours(20);
        testDay = testDay.addOvertimeHours(8); // maximum hours shared exceeded by 4 h
    }

    @Test
    public void T22_nameAbbrOfFridayEqualsFRI() throws Exception{
        testDay = testBuilder.build();
        assertEquals("FRI", testDay.nameAbbr(3));
    }

    @Test
    public void T23_nameAbbrOfFridayEqualsF() throws Exception {
        testDay = testBuilder.build();
        assertEquals("F", testDay.nameAbbr(1));
    }





}