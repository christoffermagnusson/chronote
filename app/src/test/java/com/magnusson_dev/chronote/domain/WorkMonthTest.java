package com.magnusson_dev.chronote.domain;


import com.magnusson_dev.chronote.domain.utils.WorkDayMapCreator;
import com.magnusson_dev.chronote.domain.utils.WorkMonthNamer;

import org.junit.Test;
import org.threeten.bp.YearMonth;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.*;

public class WorkMonthTest {



    YearMonth currentMonth  = YearMonth.now();
    YearMonth JANUARY       = YearMonth.now().withMonth(1);
    YearMonth FEBRUARY      = YearMonth.now().withMonth(2);
    YearMonth MARCH         = YearMonth.now().withMonth(3);
    YearMonth APRIL         = YearMonth.now().withMonth(4);
    YearMonth MAY           = YearMonth.now().withMonth(5);
    YearMonth JUNE          = YearMonth.now().withMonth(6);
    YearMonth JULY          = YearMonth.now().withMonth(7);
    YearMonth AUGUST        = YearMonth.now().withMonth(8);
    YearMonth SEPTEMBER     = YearMonth.now().withMonth(9);
    YearMonth OCTOBER       = YearMonth.now().withMonth(10);
    YearMonth NOVEMBER      = YearMonth.now().withMonth(11);
    YearMonth DECEMBER      = YearMonth.now().withMonth(12);





    WorkMonth testMonth = new WorkMonth.Builder(currentMonth).build();



    @Test
    public void T00_failingTest() throws Exception {
        boolean failed = true;
        assertTrue(failed);
    }

    @Test
    public void T01_testMonthInitialized() throws Exception {
        assertEquals(YearMonth.now(), testMonth.month());
    }

    @Test
    public void T02_nameEqualsCurrentMonthAtInstantiation() throws Exception {
        testMonth = new WorkMonth.Builder(currentMonth).build();
        assertEquals(WorkMonthNamer.nameOfMonth(currentMonth), testMonth.name());
    }

    @Test
    public void T03_daysInAprilEquals30() throws Exception {
        testMonth = new WorkMonth.Builder(APRIL).build();
        assertTrue(testMonth.days() == 30);
    }

    @Test
    public void T04_daysInDecemberEquals31() throws Exception {
        testMonth = new WorkMonth.Builder(DECEMBER).build();
        assertTrue(testMonth.days() == 31);
    }

    @Test
    public void T05_workDaysInDecemberEquals31() throws Exception {
        testMonth = new WorkMonth.Builder(DECEMBER).build();
        assertTrue(testMonth.workDays().size() == 31);
    }

    @Test
    public void T06_monthEqualsCurrentMonthIfNullIsPassed() throws Exception {
        testMonth = new WorkMonth.Builder(null).build();
        assertEquals(currentMonth, testMonth.month());
    }

    @Test
    public void T07_workdaysEqualsCurrentMonthLengthIfNullIsPassed() throws Exception {
        testMonth = new WorkMonth.Builder(null).build();
        assertTrue(testMonth.days() == currentMonth.lengthOfMonth());
    }

    @Test
    public void T08_normalHoursEquals8If8Added() throws Exception {
        Map<Integer, WorkDay> copy = testMonth.workDays();
        copy.put(1, copy.get(1).addNormalHours(8));
        testMonth = testMonth.workDays(copy);
        assertTrue(testMonth.totalNormalHours() == 8);
    }

    @Test
    public void T09_normalHoursEquals24If24AddedOver3Days() throws Exception {
        Map<Integer, WorkDay> copy = testMonth.workDays();
        copy.put(1, copy.get(1).addNormalHours(8));
        copy.put(2, copy.get(2).addNormalHours(8));
        copy.put(3, copy.get(3).addNormalHours(8));
        testMonth = testMonth.workDays(copy);
        assertTrue(testMonth.totalNormalHours() == 24);
    }

    @Test
    public void T10_totalHoursEquals24If24AddedOver3Days() throws Exception {
        Map<Integer, WorkDay> copy = testMonth.workDays();
        copy.put(1, copy.get(1).addNormalHours(8));
        copy.put(2, copy.get(2).addNormalHours(8));
        copy.put(3, copy.get(3).addNormalHours(8));
        testMonth = testMonth.workDays(copy);
        assertTrue(testMonth.totalHours() == 24);
    }

    @Test
    public void T11_overTimeHoursEquals4If4Added() throws Exception {
        Map<Integer, WorkDay> copy = testMonth.workDays();
        copy.put(1, copy.get(1).addOvertimeHours(4));
        testMonth = testMonth.workDays(copy);
        assertTrue(testMonth.totalOvertimeHours() == 4);
    }

    @Test
    public void T12_overTimeHoursEquals12If12AddedOver3Days() throws Exception {
        Map<Integer, WorkDay> copy = testMonth.workDays();
        copy.put(1, copy.get(1).addOvertimeHours(4));
        copy.put(2, copy.get(2).addOvertimeHours(4));
        copy.put(3, copy.get(3).addOvertimeHours(4));
        testMonth = testMonth.workDays(copy);
        assertTrue(testMonth.totalOvertimeHours() == 12);
    }

    @Test
    public void T13_totalHoursEquals36If36AddedInCombination() throws Exception {
        Map<Integer, WorkDay> copy = testMonth.workDays();
        copy.put(1, copy.get(1).addNormalHours(8));
        copy.put(2, copy.get(2).addNormalHours(8));
        copy.put(3, copy.get(3).addNormalHours(8));
        copy.put(1, copy.get(1).addOvertimeHours(4));
        copy.put(2, copy.get(2).addOvertimeHours(4));
        copy.put(3, copy.get(3).addOvertimeHours(4));
        testMonth = testMonth.workDays(copy);
        assertTrue(testMonth.totalHours() == 36);
    }





}
