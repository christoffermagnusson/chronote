package com.magnusson_dev.chronote.domain.utils;

import android.widget.FrameLayout;

import com.magnusson_dev.chronote.domain.WorkDay;
import com.magnusson_dev.chronote.domain.WorkMonth;

import org.junit.Test;
import org.threeten.bp.Month;
import org.threeten.bp.YearMonth;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.temporal.TemporalAccessor;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;


public class CalendarFormatterTest {



    YearMonth currentMonth  = YearMonth.now();
    YearMonth JANUARY       = YearMonth.now().withMonth(1);
    YearMonth FEBRUARY      = YearMonth.now().withMonth(2);
    YearMonth MARCH         = YearMonth.now().withMonth(3);
    YearMonth APRIL         = YearMonth.now().withMonth(4);
    YearMonth MAY           = YearMonth.now().withMonth(5);
    YearMonth JUNE          = YearMonth.now().withMonth(6);
    YearMonth JULY          = YearMonth.now().withMonth(7);
    YearMonth AUGUST        = YearMonth.now().withMonth(8);
    YearMonth SEPTEMBER     = YearMonth.now().withMonth(9);
    YearMonth OCTOBER       = YearMonth.now().withMonth(10);
    YearMonth NOVEMBER      = YearMonth.now().withMonth(11);
    YearMonth DECEMBER      = YearMonth.now().withMonth(12);





    WorkMonth testMonth = new WorkMonth.Builder(currentMonth).build();




    @Test
    public void T00_failingTest() throws Exception {
        boolean failing = false;
        assertTrue(!failing);
    }


    @Test
    public void testingDays() throws Exception {
        System.out.println(JANUARY.atDay(1).getDayOfWeek().toString());
        System.out.println(JULY.atDay(1).getDayOfWeek().toString());
        System.out.println(MAY.atDay(1).getDayOfWeek().toString());
    }

    @Test // january 2018 starts on a Monday and therefore should not be altered
    public void T01_january2018ReturnsUnalteredSet()throws Exception {
        JANUARY = JANUARY.withYear(2018); // ensures it is 01-2018, thus making test work over time.
        Map<Integer, WorkDay> workDays = WorkDayMapCreator.createWorkDayTreeMapOf(JANUARY);
        int lengthOfWorkDays = workDays.values().size();
        int lengthOfFormattedEntries = CalendarFormatter.createFormattedCalendarEntryOf(workDays).size();

        assertTrue(lengthOfWorkDays == lengthOfFormattedEntries);
    }

    @Test // may 2018 starts on a Tuesday and should therefore be altered to return an extra pre-pended Monday for the Calendarview
    public void T02_may2018ReturnsAlteredSet() throws Exception {
        MAY = MAY.withYear(2018);
        Map<Integer, WorkDay> workDays = WorkDayMapCreator.createWorkDayTreeMapOf(MAY);
        int lengthOfWorkDays = workDays.values().size();
        int lengthOfFormattedEntries = CalendarFormatter.createFormattedCalendarEntryOf(workDays).size();

        assertTrue(lengthOfWorkDays < lengthOfFormattedEntries);

    }

    @Test // may 2018 starts on Tuesday, check that first String is "M"
    public void T03_may2018StartsWithAbbrM() throws Exception {
        MAY = MAY.withYear(2018);
        Map<Integer,WorkDay> workDays = WorkDayMapCreator.createWorkDayTreeMapOf(MAY);
        List<WorkDay> values = CalendarFormatter.createFormattedCalendarEntryOf(workDays);
        assertEquals("M", values.get(0).nameAbbr(1));
    }

    @Test // may 2018 starts on Tuesday, check that second String is "T"
    public void T04_may2018SecondsWithAbbrT() throws Exception {
        MAY = MAY.withYear(2018);
        Map<Integer,WorkDay> workDays = WorkDayMapCreator.createWorkDayTreeMapOf(MAY);
        List<WorkDay> values = CalendarFormatter.createFormattedCalendarEntryOf(workDays);
        assertEquals("T", values.get(1).nameAbbr(1));
    }

    @Test // may 2018 starts on Tuesday, therefore first entry will be 30th april
    public void T05_may2018FirstEntryIsApril30() throws Exception {
        MAY = MAY.withYear(2018);
        Map<Integer, WorkDay> workDays = WorkDayMapCreator.createWorkDayTreeMapOf(MAY);
        List<WorkDay> values = CalendarFormatter.createFormattedCalendarEntryOf(workDays);
        boolean april30 = values.get(0).date().getDayOfMonth() == 30 && values.get(0).date().getMonth() == Month.APRIL;
        assertTrue(april30);
    }

}