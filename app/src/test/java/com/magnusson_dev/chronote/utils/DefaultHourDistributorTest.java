package com.magnusson_dev.chronote.utils;

import com.magnusson_dev.chronote.domain.WorkHourType;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;
import static com.magnusson_dev.chronote.domain.WorkHourType.NORMAL_HOUR;
import static com.magnusson_dev.chronote.domain.WorkHourType.OVERTIME_HOUR;


public class DefaultHourDistributorTest {


    private HourDistributor<WorkHourType, Double> testDist = DefaultHourDistributor.getInstance();
    private Map<WorkHourType, Double> testMap = null;
    private static final double ACCEPTED_DELTA = 0.001;

    private void printDoubleEval(final int testnr, final double expected, final double actual){
        System.out.println(String.format("T%d:\nexpected %f \nactual %10f",testnr,expected,actual));
    }

    @Test
    public void T00_failingTest() throws Exception {
        boolean failing = true;
        assertTrue(failing);
    }

    @Test
    public void T01_8normalOutOf10() throws Exception {
        testMap = testDist.distibutedHours(8,2);
        int total = 10;
        double expected = (double) 8 / (double) total;
        double actual = testMap.get(NORMAL_HOUR);
        printDoubleEval(1,expected,actual);
        assertEquals(expected, actual, ACCEPTED_DELTA);
    }

    @Test
    public void T02_13normalOutOf15() throws Exception {
        testMap = testDist.distibutedHours(13,2);
        int total = 15;
        double expected = (double) 13 / (double) total;
        double actual = testMap.get(NORMAL_HOUR);
        printDoubleEval(2, expected,actual);
        assertEquals(expected,actual, ACCEPTED_DELTA);
    }

    @Test
    public void T03_2overtimeOutOf15() throws Exception {
        testMap = testDist.distibutedHours(13,2);
        int total = 15;
        double expected = (double) 2 / (double) total;
        double actual = testMap.get(OVERTIME_HOUR);
        printDoubleEval(3, expected,actual);
        assertEquals(expected,actual, ACCEPTED_DELTA);
    }

    @Test
    public void T04_0divisionNotPossible() throws Exception {
        testMap = testDist.distibutedHours(0,0);
        double [] expected = {0.0,0.0};
        double [] actual = new double[expected.length];
        actual[0] = testMap.get(NORMAL_HOUR);
        actual[1] = testMap.get(OVERTIME_HOUR);
        assertArrayEquals(expected, actual, ACCEPTED_DELTA);
    }

    @Test
    public void T05_negativeValuesNotPossible() throws Exception {
        testMap = testDist.distibutedHours(-10, -10);
        double [] expected = {0.0,0.0};
        double [] actual = new double[expected.length];
        actual[0] = testMap.get(NORMAL_HOUR);
        actual[1] = testMap.get(OVERTIME_HOUR);
        assertArrayEquals(expected, actual, ACCEPTED_DELTA);
    }

    @Test
    public void T06_oneNegativeValuePossible() throws Exception {
        testMap = testDist.distibutedHours(10, -10);
        double [] expected = {1.0,0.0};
        double [] actual = new double[expected.length];
        actual[0] = testMap.get(NORMAL_HOUR);
        actual[1] = testMap.get(OVERTIME_HOUR);
        assertArrayEquals(expected, actual, ACCEPTED_DELTA);
    }



}