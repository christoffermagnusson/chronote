package com.magnusson_dev.chronote.utils;


import java.util.Map;

public interface HourDistributor<K,V>{


    Map<K,V> distibutedHours(final int normal, final int overtime);

}
