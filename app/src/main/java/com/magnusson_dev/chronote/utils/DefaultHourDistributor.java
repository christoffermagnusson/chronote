package com.magnusson_dev.chronote.utils;


import com.magnusson_dev.chronote.domain.WorkHourType;

import java.util.HashMap;
import java.util.Map;

import static com.magnusson_dev.chronote.domain.WorkHourType.NORMAL_HOUR;
import static com.magnusson_dev.chronote.domain.WorkHourType.OVERTIME_HOUR;

public final class DefaultHourDistributor implements HourDistributor<WorkHourType,Double> {

    private static DefaultHourDistributor INSTANCE = null;

    private DefaultHourDistributor(){}

    public static DefaultHourDistributor getInstance(){
        if(INSTANCE == null){
            INSTANCE = new DefaultHourDistributor();
        }

        return INSTANCE;
    }


    /**
     * Calculates percentage of total hours of some unit. Could be on a
     * WorkDay, WorkMonth and other units.
     * Takes into account zero-division by returning a default map if
     * inputs are zero
     * If one value is negative and the other is positive, (should not happen)
     * the negative value will be normalized to zero and the other value
     * will represent 100% of the distributed hours
     * @param normal
     * @param overtime
     * @return
     */
    @Override
    public Map<WorkHourType,Double> distibutedHours(final int normal, final int overtime) {
        Map<WorkHourType,Double> distributedHours = new HashMap<>();
        if(isNaN(normal,overtime) || isNegative(normal,overtime)){
            distributedHours.put(NORMAL_HOUR, 0.0);
            distributedHours.put(OVERTIME_HOUR, 0.0);
            return distributedHours;
        }else if(isPartlyNegative(normal, overtime)){
            return distributedHoursNegative(normal,overtime);
        }


        int total = normal + overtime;

        double normalPart = (double) normal / (double) total;
        distributedHours.put(NORMAL_HOUR, normalPart);

        double overtimePart = (double) overtime / (double) total;
        distributedHours.put(OVERTIME_HOUR, overtimePart);

        return distributedHours;
    }


    private boolean isNaN(final int normal, final int overtime){
        return normal == 0 && overtime == 0;
    }

    private boolean isNegative(final int normal, final int overtime){
        return normal < 0 && overtime < 0;
    }

    private boolean isPartlyNegative(final int normal, final int overtime){
        return normal < 0 || overtime < 0;
    }

    private Map<WorkHourType, Double> distributedHoursNegative(final int normal, final int overtime){
        Map<WorkHourType,Double> distributedHours = new HashMap<>();
        WorkHourType negativeValue = normal < 0 ? NORMAL_HOUR : OVERTIME_HOUR;
        switch(negativeValue){
            case NORMAL_HOUR:
                distributedHours.put(NORMAL_HOUR, 0.0);
                distributedHours.put(OVERTIME_HOUR, overtime == 0 ? 0.0 : 1.0);
                break;
            case OVERTIME_HOUR:
                distributedHours.put(NORMAL_HOUR, normal == 0 ? 0.0 : 1.0);
                distributedHours.put(OVERTIME_HOUR, 0.0);
                break;
        }
        return distributedHours;
    }
}
