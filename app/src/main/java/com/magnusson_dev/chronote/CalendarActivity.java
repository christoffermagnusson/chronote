package com.magnusson_dev.chronote;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;

import com.jakewharton.threetenabp.AndroidThreeTen;
import com.magnusson_dev.chronote.R;
import com.magnusson_dev.chronote.domain.WorkDay;
import com.magnusson_dev.chronote.domain.WorkMonth;
import com.magnusson_dev.chronote.domain.utils.CalendarFormatter;

import org.threeten.bp.YearMonth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CalendarActivity extends AppCompatActivity {

    private GridView calendarGrid = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        AndroidThreeTen.init(this);

        initGui();

    }

    private void initGui() {
        calendarGrid = findViewById(R.id.calendarGrid);
        calendarGrid.setNumColumns(7);
        // testing testing
        WorkMonth testMonth = new WorkMonth.Builder(YearMonth.now()).build();
        List<WorkDay> names = CalendarFormatter.createFormattedCalendarEntryOf(testMonth.workDays());

        calendarGrid.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names));


    }
}
