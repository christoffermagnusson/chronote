package com.magnusson_dev.chronote.domain;



import org.threeten.bp.YearMonth;

import java.util.Map;
import java.util.TreeMap;

import com.magnusson_dev.chronote.domain.utils.HourCalculator;
import com.magnusson_dev.chronote.domain.utils.HourCalculatorFactory;
import com.magnusson_dev.chronote.domain.utils.WorkDayMapCreator;
import com.magnusson_dev.chronote.domain.utils.WorkMonthNamer;

/**
 * Class representing a month in the calendar. Contains a Map with worked days and
 * days to come. Also contains methods to aggregate hour count of the month
 */
public final class WorkMonth {
    // Required parameters
    private final String name;
    private final YearMonth month;
    private final Map<Integer,WorkDay> workDays;

    /**
     * Made private to control instantiation through Builder pattern.
     * If workDays previously has been updated, then the updated days should be used.
     * @param builder
     */
    private WorkMonth(WorkMonth.Builder builder){
        this.month              = builder.month;
        this.workDays           = builder.workDays == null ? WorkDayMapCreator.createWorkDayTreeMapOf(this.month) : builder.workDays;
        this.name               = nameOfMonth(this.month);
    }

    /**
     * Builder pattern for object initialization
     */
    public static class Builder{

        // Required parameters
        private final YearMonth month;

        // Optional parameters
        private Map<Integer, WorkDay> workDays = null;

        /**
         * If parameters are null, then instantiate the month as the current month
         * by using the YearMonth.now() method.
         * @param month
         */
        public Builder(YearMonth month){
            if(month==null) month = YearMonth.now();
            this.month = month;
        }

        public Builder workDays(Map<Integer, WorkDay> workDays){
            this.workDays = workDays;
            return this;
        }


        public WorkMonth build(){
            return new WorkMonth(this);
        }



    }
    /**
     * Standard object behaviour
     */

    public String name(){
        return this.name;
    }

    private String nameOfMonth(YearMonth month) {
        return WorkMonthNamer.nameOfMonth(month);
    }

    public YearMonth month(){
        return this.month;
    }

    public int days(){
        return this.month.lengthOfMonth();
    }

    public Map<Integer,WorkDay> workDays(){
        return new TreeMap<>(workDays); // return a copy of the Map to ensure immutability when manipulating the state
    }

    public WorkMonth workDays(Map<Integer, WorkDay> updatedDays){
        return new WorkMonth.Builder(this.month).workDays(updatedDays).build();
    }

    public int totalNormalHours(){
        HourCalculator<Integer,WorkDay> normalHourCalc =
                HourCalculatorFactory.createCalculatorOf(WorkHourType.NORMAL_HOUR);
        return normalHourCalc.calculateHours(workDays);
    }

    public int totalOvertimeHours(){
        HourCalculator<Integer,WorkDay> overtimeHourCalc =
                HourCalculatorFactory.createCalculatorOf(WorkHourType.OVERTIME_HOUR);

        return overtimeHourCalc.calculateHours(workDays);
    }

    public int totalHours(){
        return this.totalNormalHours() + this.totalOvertimeHours();
    }


}
