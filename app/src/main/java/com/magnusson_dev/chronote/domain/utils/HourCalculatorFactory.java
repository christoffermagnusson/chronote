package com.magnusson_dev.chronote.domain.utils;


import android.util.Log;

import org.threeten.bp.LocalDate;

import java.util.TreeMap;

import com.magnusson_dev.chronote.domain.WorkDay;
import com.magnusson_dev.chronote.domain.WorkHourType;

public class HourCalculatorFactory {

    private static final String TAG = "HourCalculatorFactory";


    private static HourCalculator<Integer,WorkDay> NORMAL_HOUR_CALCULATOR_TREE = workDays -> {
        int current = LocalDate.now().getDayOfMonth();
        TreeMap<Integer,WorkDay> treeMap = (TreeMap<Integer,WorkDay>) workDays;
        int sumHours = 0;
        for(WorkDay val : treeMap.headMap(current, true).values()){
            sumHours += val.normalHours();
        }
        return sumHours;
    };

    private static HourCalculator<Integer, WorkDay> OVERTIME_HOUR_CALCULATOR_TREE = workDays -> {
        int current = LocalDate.now().getDayOfMonth();
        TreeMap<Integer,WorkDay> treeMap = (TreeMap<Integer,WorkDay>) workDays;
        int sumHours = 0;
        for(WorkDay val : treeMap.headMap(current, true).values()){
            sumHours += val.overtimeHours();
        }
        return sumHours;
    };


    public static HourCalculator<Integer,WorkDay> createCalculatorOf(WorkHourType type){
        HourCalculator<Integer, WorkDay> calculator = defaultCalc -> Log.d(TAG, "of: No calculator attached");
        switch(type){
            case NORMAL_HOUR:
                calculator = NORMAL_HOUR_CALCULATOR_TREE;
                break;
            case OVERTIME_HOUR:
                calculator = OVERTIME_HOUR_CALCULATOR_TREE;
                break;
        }
        return calculator;
    }



}
