package com.magnusson_dev.chronote.domain;


import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;

public class WorkDay {

    private static final int MAX_HOURS = 24;

    private final DayOfWeek dayOfWeek;
    private final int normalHours;
    private final int overtimeHours;
    private final LocalDate date;

    private WorkDay(WorkDay.Builder builder){
        this.dayOfWeek      = builder.dayOfWeek;
        this.normalHours    = builder.normalHours;
        this.overtimeHours  = builder.overtimeHours;
        this.date           = builder.date;
    }

    public static class Builder{
        // Required parameters
        private final DayOfWeek dayOfWeek;
        private final LocalDate date;
        // Optional parameters
        private int normalHours     = 0;
        private int overtimeHours   = 0;

        public Builder(final DayOfWeek dayOfWeek, final LocalDate date){
            this.dayOfWeek  = dayOfWeek;
            this.date       = date;
        }



        public Builder normalHours(int normalHours){
            if(negativeValue(normalHours)){ throw new IllegalArgumentException("Hour count cannot be negative. Using default value");}
            if(limitExceeded(normalHours)){ throw new IllegalArgumentException("Cant add more hours than 24 to one single day. Using default value"); }
            this.normalHours = normalHours;
            return this;
        }

        public Builder overtimeHours(int overtimeHours){
            if(negativeValue(overtimeHours)){ throw new IllegalArgumentException("Hour count cannot be negative. Using default value");}
            if(limitExceeded(overtimeHours)){ throw new IllegalArgumentException("Cant add more hours than 24 to one single day. Using default value"); }
            this.overtimeHours = overtimeHours;
            return this;
        }

        public WorkDay build(){
            return new WorkDay(this);
        }

        private boolean negativeValue(final int value){
            return value < 0;
        }

        /**
         * Checks whether a value above the daily limit of 24 has been passed.
         * @param value
         * @return
         */
        private boolean limitExceeded(final int value){
            return value > MAX_HOURS;
        }

    }

    /**
     * Standard object behaviour
     */

    public DayOfWeek dayOfWeek(){
        return this.dayOfWeek;
    }

    public LocalDate date(){ return this.date; }

    public int normalHours(){
        return this.normalHours;
    }

    public WorkDay addNormalHours(int normalHours){
        if(normalHours < 0){ throw new IllegalArgumentException("Hour count can't be negative. No hours added"); }
        if(sharedLimitExceeded(normalHours)) { throw new IllegalArgumentException("Total hour count exceeds 24 h. Invalid amount"); }
        return new WorkDay.Builder(this.dayOfWeek, this.date)
                        .normalHours((this.normalHours + normalHours))
                        .overtimeHours(this.overtimeHours)
                        .build();
    }


    public WorkDay subtractNormalHours(int normalHours){
        if(this.normalHours < normalHours){ throw new IllegalArgumentException("Hour count can't be negative. No hours subtracted"); }
        return new WorkDay.Builder(this.dayOfWeek, this.date)
                        .normalHours((this.normalHours - normalHours))
                        .overtimeHours(this.overtimeHours)
                        .build();
    }


    public int overtimeHours(){
        return this.overtimeHours;
    }

    public WorkDay addOvertimeHours(int overtimeHours){
        if(overtimeHours < 0){ throw new IllegalArgumentException("Hour count can't be negative. No hours added"); }
        if(sharedLimitExceeded(overtimeHours)){ throw new IllegalArgumentException("Total hour count exceeds 24 h. Invalid amount"); }
        return new WorkDay.Builder(this.dayOfWeek, this.date)
                .normalHours(this.normalHours)
                .overtimeHours((this.overtimeHours + overtimeHours))
                .build();
    }

    private boolean sharedLimitExceeded(final int value){
        return (totalHours() + value) > MAX_HOURS;
    }

    public int totalHours(){
        return this.normalHours + this.overtimeHours;
    }

    public String nameAbbr(final int limit){
        return dayOfWeek.toString().substring(0,limit);
    }


    @Override
    public String toString() {
        return String.format("%s %s", nameAbbr(3), date.toString());
    }
}
