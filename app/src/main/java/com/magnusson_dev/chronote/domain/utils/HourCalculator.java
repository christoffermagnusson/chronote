package com.magnusson_dev.chronote.domain.utils;


import java.util.Map;


public interface HourCalculator<K,V> {


    int calculateHours(Map<K, V> workDays);


}
