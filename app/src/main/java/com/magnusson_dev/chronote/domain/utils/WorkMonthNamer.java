package com.magnusson_dev.chronote.domain.utils;


import org.threeten.bp.YearMonth;

public class WorkMonthNamer {

        private static final String JANUARY     = "January";
        private static final String FEBRUARY    = "February";
        private static final String MARCH       = "Mars";
        private static final String APRIL       = "April";
        private static final String MAY         = "May";
        private static final String JUNE        = "June";
        private static final String JULY        = "July";
        private static final String AUGUST      = "August";
        private static final String SEPTEMBER   = "September";
        private static final String OCTOBER     = "October";
        private static final String NOVEMBER    = "November";
        private static final String DECEMBER    = "December";

        public static String nameOfMonth(YearMonth month){
            String name;
            switch (month.getMonth()){
                case JANUARY:
                    name = JANUARY;
                    break;
                case FEBRUARY:
                    name = FEBRUARY;
                    break;
                case MARCH:
                    name = MARCH;
                    break;
                case APRIL:
                    name = APRIL;
                    break;
                case MAY:
                    name = MAY;
                    break;
                case JUNE:
                    name = JUNE;
                    break;
                case JULY:
                    name = JULY;
                    break;
                case AUGUST:
                    name = AUGUST;
                    break;
                case SEPTEMBER:
                    name = SEPTEMBER;
                    break;
                case OCTOBER:
                    name = OCTOBER;
                    break;
                case NOVEMBER:
                    name = NOVEMBER;
                    break;
                case DECEMBER:
                    name = DECEMBER;
                    break;
                default:
                    name = "default";
            }
            return name;
        }



}
