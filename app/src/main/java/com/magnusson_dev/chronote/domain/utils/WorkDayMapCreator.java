package com.magnusson_dev.chronote.domain.utils;


import com.magnusson_dev.chronote.domain.WorkDay;

import org.threeten.bp.YearMonth;

import java.util.Map;
import java.util.TreeMap;

public class WorkDayMapCreator {

    private WorkDayMapCreator(){};

    public static Map<Integer,WorkDay> createWorkDayTreeMapOf(YearMonth month){
        Map<Integer,WorkDay> workDays = new TreeMap<>();
        for(int i=1; i<=month.lengthOfMonth(); i++){
            workDays.put(i, new WorkDay.Builder(month.atDay(i).getDayOfWeek(), month.atDay(i)).build());
        }
        return workDays;
    }


}
