package com.magnusson_dev.chronote.domain.utils;


import android.widget.ArrayAdapter;

import com.magnusson_dev.chronote.domain.WorkDay;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public class CalendarFormatter{




    // Make more generic...
    public static List<WorkDay> createFormattedCalendarEntryOf(Map<Integer,WorkDay> workDays){
        WorkDay firstDayOfMonth = workDays.get(1);
        if(firstDayOfMonthIsMonday(firstDayOfMonth)){
            return new ArrayList<>(workDays.values());
        }
        List<WorkDay> monthDays = new ArrayList<>(workDays.values());
        List<WorkDay> preformattedDays = preformatDays(firstDayOfMonth);
        preformattedDays.addAll(monthDays); // always small amount of elements added, (never over 31)
        return preformattedDays;
    }

    private static List<WorkDay> preformatDays(final WorkDay firstDayOfMonth){
        List<WorkDay> preformattedDays;
        switch(firstDayOfMonth.dayOfWeek()){
            case TUESDAY:
                preformattedDays = Collections.singletonList(buildDay(DayOfWeek.MONDAY, firstDayOfMonth.date().minusDays(1)));
                break;
            case WEDNESDAY:
                preformattedDays = Arrays.asList(buildDay(DayOfWeek.MONDAY, firstDayOfMonth.date().minusDays(2)),
                        buildDay(DayOfWeek.TUESDAY, firstDayOfMonth.date().minusDays(1)));
                break;
            case THURSDAY:
                preformattedDays = Arrays.asList(buildDay(DayOfWeek.MONDAY, firstDayOfMonth.date().minusDays(3)),
                        buildDay(DayOfWeek.TUESDAY, firstDayOfMonth.date().minusDays(2)),
                        buildDay(DayOfWeek.WEDNESDAY, firstDayOfMonth.date().minusDays(1)));
                break;
            case FRIDAY:
                preformattedDays = Arrays.asList(buildDay(DayOfWeek.MONDAY, firstDayOfMonth.date().minusDays(4)),
                        buildDay(DayOfWeek.TUESDAY, firstDayOfMonth.date().minusDays(3)),
                        buildDay(DayOfWeek.WEDNESDAY, firstDayOfMonth.date().minusDays(2)),
                        buildDay(DayOfWeek.THURSDAY, firstDayOfMonth.date().minusDays(1)));
                break;
            case SATURDAY:
                preformattedDays = Arrays.asList(buildDay(DayOfWeek.MONDAY, firstDayOfMonth.date().minusDays(5)),
                        buildDay(DayOfWeek.TUESDAY, firstDayOfMonth.date().minusDays(4)),
                        buildDay(DayOfWeek.WEDNESDAY, firstDayOfMonth.date().minusDays(3)),
                        buildDay(DayOfWeek.THURSDAY, firstDayOfMonth.date().minusDays(2)),
                        buildDay(DayOfWeek.FRIDAY, firstDayOfMonth.date().minusDays(1)));
                break;
            case SUNDAY:
                preformattedDays = Arrays.asList(buildDay(DayOfWeek.MONDAY, firstDayOfMonth.date().minusDays(6)),
                        buildDay(DayOfWeek.TUESDAY, firstDayOfMonth.date().minusDays(5)),
                        buildDay(DayOfWeek.WEDNESDAY, firstDayOfMonth.date().minusDays(4)),
                        buildDay(DayOfWeek.THURSDAY, firstDayOfMonth.date().minusDays(3)),
                        buildDay(DayOfWeek.FRIDAY, firstDayOfMonth.date().minusDays(2)),
                        buildDay(DayOfWeek.SATURDAY, firstDayOfMonth.date().minusDays(1)));
                break;
            default:
                preformattedDays = Collections.emptyList();
        }
        return new ArrayList<>(preformattedDays);
    }

    private static WorkDay buildDay(final DayOfWeek dayOfWeek, final LocalDate date){
        return new WorkDay.Builder(dayOfWeek,date).build();
    }


    private static boolean firstDayOfMonthIsMonday(final WorkDay firstDayOfMonth){
        return firstDayOfMonth.dayOfWeek() == DayOfWeek.MONDAY;
    }




}
